<?php

namespace App\Http\Services;


use Symfony\Component\HttpFoundation\Request;

class DataProviderX implements ReadJasonProviderInterface
{

    public function readJason(){

        $jsonString = file_get_contents('assets/DataProvider/DataProviderX.json');

        $data = json_decode($jsonString, true);

        return $data;
    }

}
