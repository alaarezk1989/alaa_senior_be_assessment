<?php

namespace App\Http\Services;


use Symfony\Component\HttpFoundation\Request;

class DataProviderY implements ReadJasonProviderInterface
{

    public function readJason()
    {
        $jsonString = file_get_contents('assets/DataProvider/DataProviderY.json');

        $data = json_decode($jsonString, true);

        return $data;
    }

}
