<?php

namespace App\Http\Services;

class ReadJasonData
{

    public $dataProvider;

    public function setOutput(ReadJasonProviderInterface $dataProviderType)
    {
        $this->dataProvider = $dataProviderType;
    }

    public function loadOutput()
    {
        return $this->dataProvider->readJason();
    }

}
