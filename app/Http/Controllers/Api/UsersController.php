<?php

namespace App\Http\Controllers\Api;

use App\Http\Services\ReadJasonData;
use App\Repositories\UserRepository;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpFoundation\Request;

class UsersController extends Controller
{
    protected $readJasonData;
    protected $userRepository;

    public function __construct(ReadJasonData $readJasonData, UserRepository $userRepository)
    {
        $this->readJasonData = $readJasonData;
        $this->userRepository = $userRepository;
    }

    public function index(Request $request)
    {
        $data = $this->userRepository->searchDataProvider($request);
        return $data;

    }

    public function nonCustomDataProviderX(Request $request)
    {
        $provider = $request->get('provider');
        $data = $this->userRepository->nonCustomDataProviderX($request, $provider)->get();

        return $data;
    }

}
