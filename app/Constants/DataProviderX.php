<?php

namespace App\Constants;

final class DataProviderX
{
    const authorised = 1;
    const  decline = 2;
    const  refunded = 3;

    public static function getList()
    {
        return [
            DataProviderX::authorised => 'authorised',
            DataProviderX::decline => 'decline',
            DataProviderX::refunded => 'refunded',
        ];
    }

    public static function getKeyList()
    {
        return [
             'authorised'=>DataProviderX::authorised ,
             'decline'=>DataProviderX::decline ,
             'refunded'=>DataProviderX::refunded ,
        ];
    }

    public static function getOne($index = '')
    {
        $list = self::getKeyList();
        $listOne = '';
        if ($index) {
            $listOne = $list[$index];
        }
        return $listOne;
    }

}
