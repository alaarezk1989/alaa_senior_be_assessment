<?php

namespace App\Constants;

final class DataProviderY
{

    const authorised = 100;
    const  decline = 200;
    const  refunded = 300;

    public static function getList()
    {
        return [
            DataProviderY::authorised => 'authorised',
            DataProviderY::decline => 'decline',
            DataProviderY::refunded => 'refunded',
        ];
    }

    public static function getKeyList()
    {
        return [
            'authorised' => DataProviderY::authorised,
            'decline' => DataProviderY::decline,
            'refunded' => DataProviderY::refunded,
        ];
    }

    public static function getOne($index = '')
    {
        $list = self::getKeyList();
        $listOne = '';
        if ($index) {
            $listOne = $list[$index];
        }
        return $listOne;
    }

}
