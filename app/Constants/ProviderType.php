<?php

namespace App\Constants;

final class ProviderType
{

    const DATA_PROVIDER_X = "DataProviderX";
    const  DATA_PROVIDER_Y = "DataProviderY";

    public static function getList()
    {
        return [
            ProviderType::DATA_PROVIDER_X => 'DataProviderX',
            ProviderType::DATA_PROVIDER_Y => 'DataProviderY',
        ];
    }

    public static function getKeyList()
    {
        return [
            'DataProviderX' => ProviderType::DATA_PROVIDER_X,
            'DataProviderY' => ProviderType::DATA_PROVIDER_Y,
        ];
    }

    public static function getOne($index = '')
    {
        $list = self::getKeyList();
        $listOne = '';
        if ($index) {
            $listOne = $list[$index];
        }
        return $listOne;
    }

}
