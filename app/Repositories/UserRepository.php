<?php

namespace App\Repositories;

use App\Constants\ProviderType;
use App\Http\Services\ReadJasonData;
use Symfony\Component\HttpFoundation\Request;
use App\Constants\DataProviderX as DataProviderX;
use App\Constants\DataProviderY as DataProviderY;
use Nahid\JsonQ\Jsonq;

class UserRepository
{

    protected $readJasonData;

    public function __construct(ReadJasonData $readJasonData)
    {
        $this->readJasonData = $readJasonData;
    }

    public function searchDataProvider(Request $request)
    {
        if ($request->get('provider') && in_array($request->get('provider'), ProviderType::getList())) {
            $provider = $request->get('provider');
            $providerService = '\App\Http\Services\\' . $provider;
            $this->readJasonData->setOutput(new $providerService());
            $users = $this->readJasonData->loadOutput()['users'];
        } else {
            $users = array();
            foreach (ProviderType::getList() as $key => $provider) {
                $providerService = '\App\Http\Services\\' . $provider;
                $this->readJasonData->setOutput(new $providerService());
                $users = array_merge($users, $this->readJasonData->loadOutput()['users']);
            }
        }


        if ($request->get('statusCode') && !empty($request->get('statusCode'))) {
            $users = array_filter($users, function ($item) use ($request) {
                if ((isset($item['statusCode']) && $item['statusCode'] == DataProviderX::getOne($request->get('statusCode'))) || isset($item['status']) && $item['status'] == DataProviderY::getOne($request->get('statusCode'))) {
                    return true;
                }
            });
        }


        if ($request->get('Currency') && !empty($request->get('Currency'))) {
            $users = array_filter($users, function ($item) use ($request) {
                if ((isset($item['Currency']) && $item['Currency'] == $request->get('Currency')) || isset($item['currency']) && $item['currency'] == $request->get('Currency')) {
                    return true;
                }

            });
        }

        if ($request->has('balanceMin') && !empty($request->get('balanceMin'))) {
            $users = array_filter($users, function ($item) use ($request) {
                if (isset($item['balance']) && $item['balance'] >= $request->get('balanceMin')) {
                    return true;
                }

            });

        }

        if ($request->has('balanceMax') && !empty($request->get('balanceMax'))) {
            $users = array_filter($users, function ($item) use ($request) {
                if (isset($item['balance']) && $item['balance'] <= $request->get('balanceMax')) {
                    return true;
                }

            });
        }
        return $users;
    }


    public function nonCustomDataProviderX(Request $request, $provider)
    {

        $query = new Jsonq('assets/DataProvider/' . $provider . '.json');

        $users = $query->from('users');

        if ($request->get('statusCode') && !empty($request->get('statusCode'))) {
            $users->where('statusCode', '=', DataProviderX::getOne($request->get('statusCode')));
        }
//
        if ($request->get('currency') && !empty($request->get('currency'))) {
            $users->where('Currency', '=', $request->get('currency'));
        }

        if ($request->has('balanceMin') && !empty($request->get('balanceMin'))) {
            $users->where('balance', '>=', $request->get('balanceMin'));
        }
        if ($request->has('balanceMax') && !empty($request->get('balanceMax'))) {
            $users->where('balance', '<=', $request->get('balanceMax'));
        }

        return $users;
    }

}
